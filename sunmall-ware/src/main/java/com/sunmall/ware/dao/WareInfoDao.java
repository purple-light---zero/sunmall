package com.sunmall.ware.dao;

import com.sunmall.ware.entity.WareInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 仓库信息
 * 
 * @author yangqiao
 * @email 1498065149@qq.com
 * @date 2023-04-06 11:33:27
 */
@Mapper
public interface WareInfoDao extends BaseMapper<WareInfoEntity> {
	
}
