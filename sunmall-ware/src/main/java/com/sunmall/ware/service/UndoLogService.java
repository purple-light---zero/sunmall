package com.sunmall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.common.utils.PageUtils;
import com.sunmall.ware.entity.UndoLogEntity;

import java.util.Map;

/**
 * 
 *
 * @author yangqiao
 * @email 1498065149@qq.com
 * @date 2023-04-06 11:33:27
 */
public interface UndoLogService extends IService<UndoLogEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

