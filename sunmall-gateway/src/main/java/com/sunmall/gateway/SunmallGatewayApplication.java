package com.sunmall.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * 开启服务注册发现
 * (配置nacos的注册中心地址)
 * 编写网关配置文件
 */
@EnableDiscoveryClient
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class SunmallGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(SunmallGatewayApplication.class, args);
    }

}
