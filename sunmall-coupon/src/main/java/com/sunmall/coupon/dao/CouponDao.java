package com.sunmall.coupon.dao;

import com.sunmall.coupon.entity.CouponEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 优惠券信息
 * 
 * @author yangqiao
 * @email 1498065149@qq.com
 * @date 2023-04-06 11:05:55
 */
@Mapper
public interface CouponDao extends BaseMapper<CouponEntity> {
	
}
