package com.sunmall.product;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;


@EnableDiscoveryClient
@MapperScan("com.sunmall.product.dao")
@SpringBootApplication
public class SunmallProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(SunmallProductApplication.class, args);
    }

}
