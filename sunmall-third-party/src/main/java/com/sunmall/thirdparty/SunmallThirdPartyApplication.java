package com.sunmall.thirdparty;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SunmallThirdPartyApplication {

    public static void main(String[] args) {
        SpringApplication.run(SunmallThirdPartyApplication.class, args);
    }

}
