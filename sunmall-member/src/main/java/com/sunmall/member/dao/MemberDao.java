package com.sunmall.member.dao;

import com.sunmall.member.entity.MemberEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员
 * 
 * @author yangqiao
 * @email 1498065149@qq.com
 * @date 2023-04-06 11:19:25
 */
@Mapper
public interface MemberDao extends BaseMapper<MemberEntity> {
	
}
